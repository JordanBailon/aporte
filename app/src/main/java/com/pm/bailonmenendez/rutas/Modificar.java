package com.pm.bailonmenendez.rutas;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class Modificar extends AppCompatActivity {

    private RutasMode1 rutasMode1;
    private EditText etname, etcourse, etemail, etphone, ettiempo;
    private Button btnupdate, btndelete;
    private DatabaseHelper databaseHelper;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_modificar_rutas);

        Intent intent = getIntent();
        rutasMode1 = (RutasMode1) intent.getSerializableExtra("teachers");

        databaseHelper = new DatabaseHelper(this);

        etname = (EditText) findViewById(R.id.etname);
        etcourse = (EditText) findViewById(R.id.etcourse);
        etemail = (EditText) findViewById(R.id.etemail);
        etphone = (EditText) findViewById(R.id.etphone);
        ettiempo = (EditText) findViewById(R.id.ettiempo);
        btndelete = (Button) findViewById(R.id.btndelete);
        btnupdate = (Button) findViewById(R.id.btnupdate);

        etname.setText(rutasMode1.getCourse());
        etcourse.setText(rutasMode1.getName());
        etemail.setText(rutasMode1.getPhone());
        etphone.setText(rutasMode1.getTiempo());
        ettiempo.setText(rutasMode1.getEmail());

        btnupdate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                databaseHelper.updateTeachers(rutasMode1.getId(),etname.getText().toString(),etcourse.getText().toString(),etemail.getText().toString(), etphone.getText().toString(), ettiempo.getText().toString());
                Toast.makeText(Modificar.this, "Actualizacion correcta!", Toast.LENGTH_SHORT).show();
                Intent intent = new Intent(Modificar.this,MainActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(intent);
            }
        });

        btndelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                databaseHelper.deleteUSer(rutasMode1.getId());
                Toast.makeText(Modificar.this, "Eliminacion correcta!", Toast.LENGTH_SHORT).show();
                Intent intent = new Intent(Modificar.this,MainActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(intent);
            }
        });

    }
}

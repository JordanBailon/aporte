package com.pm.bailonmenendez.rutas;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

import java.util.ArrayList;

public class ModRutas extends AppCompatActivity {

    private ListView listView;
    private ArrayList<RutasMode1> rutasMode1ArrayList;
    private CustomMod customMod;
    private DatabaseHelper databaseHelper;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_mod_rutas);

        listView = (ListView) findViewById(R.id.teachers_lvi);

        databaseHelper = new DatabaseHelper(this);

        rutasMode1ArrayList = databaseHelper.getAllTeachers();

        customMod = new CustomMod(this, rutasMode1ArrayList);
        listView.setAdapter(customMod);

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Intent intent = new Intent(ModRutas.this, Modificar.class);
                intent.putExtra("teachers", rutasMode1ArrayList.get(position));
                startActivity(intent);
            }
        });
    }
}

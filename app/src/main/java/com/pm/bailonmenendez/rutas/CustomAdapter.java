package com.pm.bailonmenendez.rutas;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.ArrayList;


public class CustomAdapter extends BaseAdapter {

    private Context context;
    private ArrayList<RutasMode1> rutasMode1ArrayList;

    public CustomAdapter(Context context, ArrayList<RutasMode1> rutasMode1ArrayList) {

        this.context = context;
        this.rutasMode1ArrayList = rutasMode1ArrayList;
    }


    @Override
    public int getCount() {
        return rutasMode1ArrayList.size();
    }

    @Override
    public Object getItem(int position) {
        return rutasMode1ArrayList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder;

        if (convertView == null) {
            holder = new ViewHolder();
            LayoutInflater inflater = (LayoutInflater) context
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(R.layout.model_teachers, null, true);

            holder.tvname = (TextView) convertView.findViewById(R.id.teachers_name);
            holder.tvcourse = (TextView) convertView.findViewById(R.id.teachers_course);
            holder.tvemail = (TextView) convertView.findViewById(R.id.teachers_email);
            holder.tvtiempo = (TextView) convertView.findViewById(R.id.teachers_tiempo);
            holder.tvphone = (TextView) convertView.findViewById(R.id.teachers_phone);

            convertView.setTag(holder);
        }else {
            // the getTag returns the viewHolder object set as a tag to the view
            holder = (ViewHolder)convertView.getTag();
        }

        holder.tvname.setText("Name: "+ rutasMode1ArrayList.get(position).getName());
        holder.tvcourse.setText("Course: "+ rutasMode1ArrayList.get(position).getCourse());
        holder.tvemail.setText("Email: "+ rutasMode1ArrayList.get(position).getEmail());
        holder.tvphone.setText("Phone"+ rutasMode1ArrayList.get(position).getPhone());
        holder.tvtiempo.setText("Tiempo"+ rutasMode1ArrayList.get(position).getTiempo());
        return convertView;
    }

    private class ViewHolder {

        protected TextView tvname, tvcourse, tvemail, tvphone, tvtiempo;
    }

}
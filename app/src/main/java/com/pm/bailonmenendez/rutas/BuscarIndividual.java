package com.pm.bailonmenendez.rutas;

import android.app.Activity;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

public class BuscarIndividual extends Activity {
Button btnbusc;
EditText txtbuscar;
TextView e1,e2,e3,e4,e5;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_buscarindividual);
        btnbusc = (Button) findViewById(R.id.btnbus);
        final DatabaseHelper helperbd = new DatabaseHelper(getApplicationContext());
        txtbuscar = (EditText)findViewById(R.id.txtbuscar);
        btnbusc.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                SQLiteDatabase db = helperbd.getReadableDatabase();
                String[] lectura = {txtbuscar.getText().toString()};
                String[] projection = {"course", "email","phone","tiempo"};
                Cursor c = db.query("teachers", projection, "name" + "=?", lectura, null, null, null);

                c.moveToFirst();
                e1.setText(c.getString(0));
                e2.setText(c.getString(1));
                e3.setText(c.getString(2));
                e4.setText(c.getString(3));

            }
        });
    }
}

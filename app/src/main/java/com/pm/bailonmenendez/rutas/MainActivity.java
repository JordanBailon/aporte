package com.pm.bailonmenendez.rutas;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class MainActivity extends Activity {

    Button agregarrutas;
    Button btn_lista;
    Button btn_buscar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        agregarrutas= (Button)findViewById(R.id.btn_agregarrutas);
        agregarrutas.setOnClickListener(new View.OnClickListener() {
            @Override


            public void onClick(View v) {
                Intent agregarrutas = new Intent(MainActivity.this, AgregarRutas.class);
                startActivity(agregarrutas);

            }
        });
        btn_lista= (Button)findViewById(R.id.btn_lista);
        btn_lista.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent lista = new Intent(MainActivity.this, ModRutas.class);
                startActivity(lista);

            }
        });
        btn_buscar= (Button)findViewById(R.id.btn_buscar);
        btn_buscar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent lista = new Intent(MainActivity.this, BuscarIndividual.class);
                startActivity(lista);

            }
        });

    }
    }

